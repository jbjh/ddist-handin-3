import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.ExportException;
import java.rmi.server.UnicastRemoteObject;
import javax.swing.*;
import javax.swing.text.*;
import javax.swing.border.TitledBorder;

public class DistributedTextEditor extends JFrame {
	private JTextArea textEditor = new JTextArea(20,0);
	private JTextField ipaddressfield = new JTextField("localhost");     
	private JTextField portNumberfield = new JTextField("portnumber here");  
	private JFileChooser dialog = new JFileChooser(System.getProperty("user.dir"));
	private DefaultListModel<String> list = new DefaultListModel<String>();
	private JList<String> username = new JList<String>(list);   
	private String currentFile = "Untitled";
	private boolean changed = false;

	private EventReplayer er;
	private Thread ert;   
	private HistoryHandler myHistoryHandler = new HistoryHandler(); 
	private DocumentEventCapturer dec = new DocumentEventCapturer(myHistoryHandler);

	private Registry peerRegistry;
	private Registry serverRegistry;

	protected boolean listen;

	InetAddress localhost;
	protected ServerSocket serverSocket;
	protected Socket clientSocket;
	protected int defaultPortNumber = 40304;
	protected int portNumber;

	RmiInterface server;
	RmiInterface client;
	RmiInterface serverObj;
	/**
	 * Main constructor
	 * Creates the main GUI
	 */
	public DistributedTextEditor() {
		textEditor.setDocument(new TextAreaDocument());
		setPreferredSize(new Dimension(640, 480));
		textEditor.setFont(new Font("Monospaced",Font.PLAIN,12));
		((AbstractDocument)textEditor.getDocument()).setDocumentFilter(dec);

		Container content = getContentPane();
		getContentPane().setLayout(new BorderLayout(0, 0));

		JScrollPane scrollarea_texteditor = 
				new JScrollPane(textEditor, 
						JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
						JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollarea_texteditor.setBorder(new TitledBorder(null, "Text editor", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		content.add(scrollarea_texteditor, BorderLayout.CENTER);
		username.setPreferredSize(new Dimension(20, 20));

		username.setFont(new Font("Monospaced",Font.PLAIN,12));
		JScrollPane scrollarea_username = 
				new JScrollPane(username, 
						ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
						ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollarea_username.setBorder(new TitledBorder(null, "Users", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		content.add(scrollarea_username, BorderLayout.EAST);	

		JPanel panel_variables = new JPanel();
		getContentPane().add(panel_variables, BorderLayout.SOUTH);
		panel_variables.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));

		usernameField = new JTextField();
		usernameField.setText("Username here");
		panel_variables.add(usernameField);
		usernameField.setColumns(14);
		ipaddressfield.setColumns(14);
		panel_variables.add(ipaddressfield);
		portNumberfield.setColumns(14);
		panel_variables.add(portNumberfield);

		JMenuBar JMB = new JMenuBar();
		setJMenuBar(JMB);
		JMenu file = new JMenu("File");
		JMenu edit = new JMenu("Edit");
		JMB.add(file); 
		JMB.add(edit);

		file.add(Listen);
		file.add(Connect);
		file.add(Disconnect);
		file.addSeparator();
		file.add(Save);
		file.add(SaveAs);
		file.add(Quit);

		edit.add(Copy);
		edit.add(Paste);
		edit.getItem(0).setText("Copy");
		edit.getItem(1).setText("Paste");

		Save.setEnabled(false);
		SaveAs.setEnabled(false);
		Disconnect.setEnabled(false);

		setDefaultCloseOperation(EXIT_ON_CLOSE);
		pack();
		textEditor.addKeyListener(k1);
		setTitle("Disconnected");
		setVisible(true);
	}

	/**
	 * KeyListener attached to the input JTextArea.
	 */
	private KeyListener k1 = new KeyAdapter() {
		public void keyPressed(KeyEvent e) {
			changed = true;
			Save.setEnabled(true);
			SaveAs.setEnabled(true);
		}
	};

	/**
	 * Helper method 
	 * Sets port number, to specified or reverts to default.
	 * @NumberFormatException - Thrown if no Integer can be parsed in the portNumberField.
	 */ 
	private void setPortNumber (){
		try{
			portNumber = Integer.parseInt(portNumberfield.getText()); //Throws NumberFormatException.
			if(portNumber > 65535 || portNumber < 1){ //Checks if portNumber is in range of the value limits.
				portNumber = defaultPortNumber;
			}
		} catch (NumberFormatException nfe){
			//			nfe.printStackTrace();
			portNumber = defaultPortNumber; //Reverts to default port.
		}
	}

	/**
	 * Thread starter method, starts a new thread of the EventReplayer class.
	 * @param client 
	 * @param socket, containing the socket from connecting or listening.
	 */
	protected void startEventReplayer(RmiInterface client){
		er = new EventReplayer(dec, client, myHistoryHandler);
		ert = new Thread(er);
		ert.start();

		Pinger ping = new Pinger(client);
		Thread pingt = new Thread(ping);
		pingt.start();
	}

	/**
	 * Menu item - Listen
	 */
	Action Listen = new AbstractAction("Listen") {
		public void actionPerformed(ActionEvent e) {
			new Thread(){ //New thread to listen for incoming connections.
				public void run(){
					try{
						Disconnect.setEnabled(true);
						Listen.setEnabled(false);
						Connect.setEnabled(false);
						setPortNumber();
						textEditor.setText("");
						dec.eventHistory.clear();
						myHistoryHandler.setIndex(InetAddress.getLocalHost().getHostAddress()+":"+portNumber);
						serverObj = new RmiImpl(usernameField.getText(), textEditor, list, myHistoryHandler);
						serverRegistry = LocateRegistry.createRegistry(portNumber);
						serverRegistry.bind("RmiInterface", serverObj);
						setTitle("I'm listening on "+InetAddress.getLocalHost().getHostAddress()+":"+portNumber);
						serverObj.addUser(serverObj.getName());
						serverObj.addPeer(serverObj);
						startEventReplayer(serverObj);
					}
					catch(Exception ex){
						ex.printStackTrace();
					}
				}
			}.start();
		}
	};

	/**
	 * Menu item - Connect
	 */
	Action Connect = new AbstractAction("Connect") {
		public void actionPerformed(ActionEvent e) {
			new Thread(){ //New thread to listen for incoming connections.
				private boolean notConnected = true;
				private int i = 0;
				public void run(){
					while(notConnected){
						try{
							Disconnect.setEnabled(true);
							Connect.setEnabled(false);
							Listen.setEnabled(false);
							setPortNumber();
							textEditor.setText("");
							myHistoryHandler.clearEventHistory();
							dec.eventHistory.clear();
							myHistoryHandler.setIndex(InetAddress.getLocalHost().getHostAddress()+":"+portNumber);
							peerRegistry = LocateRegistry.createRegistry(portNumber+i);
							client = new RmiImpl(usernameField.getText(), textEditor, list, myHistoryHandler);
							peerRegistry.bind("RmiInterface", client);
							server = (RmiInterface)Naming.lookup("rmi://"+ipaddressfield.getText()+":"+(portNumber)+"/RmiInterface");
							setTitle("Connected to " + ipaddressfield.getText() + ":" + portNumber + " - I'm " + InetAddress.getLocalHost().getHostAddress()+":"+(portNumber+i));
							notConnected = false;
							for(RmiInterface c : server.getPeers()){
								if(client.getName().equals(c.getName())){
									throw new DuplicateUsernameException("Username duplicate");
								}
							}
							for(RmiInterface c : server.getPeers()){
								//Add me to everyones list
								c.addPeer(client);
								//Ask everyone to update.
								c.updateUserList();
								//Add everyone to my user list
								client.addPeer(c);
							}
							client.addPeer(client);
							client.updateUserList();

							//Set history for new connect
							((TextAreaDocument) textEditor.getDocument()).flipFilter();
							textEditor.append(server.getHistory());
							((TextAreaDocument) textEditor.getDocument()).flipFilter();
							//Updating later, when eventQueue is done and object initialized.
							SwingUtilities.invokeLater(new Runnable() {
								public void run() {
									username.revalidate(); 
									username.updateUI();
								}
							});
							startEventReplayer(client);
						}
						catch(ExportException ee){
							notConnected = true;
							i++;
						}
						catch(DuplicateUsernameException due){
							setTitle("Disconnected - Username already in use, try another!");
							Disconnect.setEnabled(false);
							Connect.setEnabled(true);
							Listen.setEnabled(true);
						}
						catch(Exception ex){
							ex.printStackTrace();
						}
					}
				}
			}.start();
		}
	};

	/**
	 * Menu item - Disconnect
	 */
	Action Disconnect = new AbstractAction("Disconnect") {
		public void actionPerformed(ActionEvent e) {	
			setTitle("Disconnected");
			try {
				Disconnect.setEnabled(false);
				Connect.setEnabled(true);
				Listen.setEnabled(true);
				if(client == null){
					serverObj.removePeer(serverObj);
					for(RmiInterface c : serverObj.getPeers()){
						c.removePeer(serverObj);
						c.removeUser(serverObj.getName());
					}
					serverObj.removeAll();
					er.interrupt();
					serverRegistry.unbind("RmiInterface");
					UnicastRemoteObject.unexportObject(serverRegistry, true);
				} else {
					client.removePeer(client);
					for(RmiInterface c : client.getPeers()){
						c.removePeer(client);
						c.removeUser(client.getName());
					}
					client.removeAll();
					er.interrupt();
					peerRegistry.unbind("RmiInterface");
					UnicastRemoteObject.unexportObject(peerRegistry, true);
				}
				textEditor.setText("");
				myHistoryHandler.clearEventHistory();
				myHistoryHandler.clearClock();


			} catch (Exception ex) { //Catches all exception.
				ex.printStackTrace();
			}
		}
	};

	/**
	 * Menu item - Save
	 */
	Action Save = new AbstractAction("Save") {
		public void actionPerformed(ActionEvent e) {
			if(!currentFile.equals("Untitled"))
				saveFile(currentFile);
			else
				saveFileAs();
		}
	};

	/**
	 * Menu item - SaveAs
	 */
	Action SaveAs = new AbstractAction("Save as...") {
		public void actionPerformed(ActionEvent e) {
			saveFileAs();
		}
	};

	/**
	 * Menu item - Quit
	 */
	Action Quit = new AbstractAction("Quit") {
		public void actionPerformed(ActionEvent e) {
			saveOld();
			System.exit(0);
		}
	};

	ActionMap m = textEditor.getActionMap();

	Action Copy = m.get(DefaultEditorKit.copyAction);
	Action Paste = m.get(DefaultEditorKit.pasteAction);
	private JTextField usernameField;

	private void saveFileAs() {
		if(dialog.showSaveDialog(null)==JFileChooser.APPROVE_OPTION)
			saveFile(dialog.getSelectedFile().getAbsolutePath());
	}

	private void saveOld() {
		if(changed) {
			if(JOptionPane.showConfirmDialog(this, "Would you like to save "+ currentFile +" ?","Save",JOptionPane.YES_NO_OPTION)== JOptionPane.YES_OPTION)
				saveFile(currentFile);
		}
	}

	private void saveFile(String fileName) {
		try {
			FileWriter w = new FileWriter(fileName);
			textEditor.write(w);
			w.close();
			currentFile = fileName;
			changed = false;
			Save.setEnabled(false);
		}
		catch(IOException e) {
		}
	}

	/**
	 * Main method. Initializes the GUI.
	 * @param arg
	 */
	public static void main(String[] arg) {
		//Sets up the java security policy for the RMI.
		System.setProperty("java.security.policy", "./security.policy");
		new DistributedTextEditor();
	} 
}
