import java.util.concurrent.LinkedBlockingQueue;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

/**
 *
 * This class captures and remembers the text events of the given document on
 * which it is put as a filter. Normally a filter is used to put restrictions
 * on what can be written in a buffer. In out case we just use it to see all
 * the events and make a copy.
 *
 * @author Jesper Buus Nielsen
 *
 */
public class DocumentEventCapturer extends DocumentFilter {
	HistoryHandler myHistoryHandler = new HistoryHandler();
	public DocumentEventCapturer(HistoryHandler myHistoryHandler){
		this.myHistoryHandler = myHistoryHandler;
	}
	/*
	 * We are using a blocking queue for two reasons:
	 * 1) They are thread safe, i.e., we can have two threads add and take elements
	 * at the same time without any race conditions, so we do not have to do
	 * explicit synchronization.
	 * 2) It gives us a member take() which is blocking, i.e., if the queue is
	 * empty, then take() will wait until new elements arrive, which is what
	 * we want, as we then don't need to keep asking until there are new elements.
	 */
	protected LinkedBlockingQueue<MyTextEvent> eventHistory = new LinkedBlockingQueue<MyTextEvent>();

	/**
	 * If the queue is empty, then the call will block until an element arrives.
	 * If the thread gets interrupted while waiting, we throw InterruptedException.
	 *
	 * @return Head of the recorded event queue.
	 */
	MyTextEvent take() throws InterruptedException {
		return eventHistory.take();
	}

	public void insertString(FilterBypass fb, int offset,
			String str, AttributeSet a)
					throws BadLocationException {
		
		TextInsertEvent myInsertEvent = new TextInsertEvent(offset, str);
		myHistoryHandler.incrementTime();
		myInsertEvent.setTime(myHistoryHandler.getTime());
		myInsertEvent.setSender(myHistoryHandler.getIndex());
		myHistoryHandler.appendEvent(myInsertEvent);
		/* Queue a copy of the event and then modify the textarea */
		eventHistory.add(myInsertEvent); 
		super.insertString(fb, offset, str, a);
	}   

	public void remove(FilterBypass fb, int offset, int length)
			throws BadLocationException {
		TextRemoveEvent myTextRemoveEvent = new TextRemoveEvent(offset,length);
		myHistoryHandler.incrementTime();
		myTextRemoveEvent.setSender(myHistoryHandler.getIndex());
		myTextRemoveEvent.setTime(myHistoryHandler.getTime());
		myHistoryHandler.appendEvent(myTextRemoveEvent);
		
		/* Queue a copy of the event and then modify the textarea */
		eventHistory.add(myTextRemoveEvent);  
		super.remove(fb, offset, length);
	}

	public void replace(FilterBypass fb, int offset,
			int length,
			String str, AttributeSet a)
					throws BadLocationException {

		/* Queue a copy of the event and then modify the text */
		if (length > 0) {
			TextRemoveEvent myTextRemoveEvent = new TextRemoveEvent(offset,length);
			myHistoryHandler.incrementTime();
			myTextRemoveEvent.setSender(myHistoryHandler.getIndex());
			myTextRemoveEvent.setTime(myHistoryHandler.getTime());
			myHistoryHandler.appendEvent(myTextRemoveEvent);
			eventHistory.add(myTextRemoveEvent);  
		}  
		TextInsertEvent myTextInsertEvent = new TextInsertEvent(offset, str);
		myHistoryHandler.incrementTime();
		myTextInsertEvent.setTime(myHistoryHandler.getTime());
		myTextInsertEvent.setSender(myHistoryHandler.getIndex());
		myHistoryHandler.appendEvent(myTextInsertEvent);
		eventHistory.add(myTextInsertEvent); 
		super.replace(fb, offset, length, str, a);
	}
}