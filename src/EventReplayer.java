import java.rmi.ConnectException;
import java.rmi.RemoteException;

public class EventReplayer implements Runnable {

	private DocumentEventCapturer dec;
	private RmiInterface mySelf;
	private RmiInterface disconnectedPeer;
	private MyTextEvent lostEvent;
	private boolean interrupted;

	public EventReplayer(DocumentEventCapturer dec, RmiInterface mySelf, HistoryHandler historyHandler) {
		this.dec = dec;
		this.mySelf = mySelf; //My self, instance of client or server
		interrupted = false;
	}
	/**
	 * Used to interrupt the thread
	 */
	public void interrupt(){
		interrupted = true;
	}
	public void run() {
		MyTextEvent mte = null;
		while (!interrupted) {
			try {
				mte = dec.take(); //Get the head of the event queue.
				for(RmiInterface c : mySelf.getPeers()){
					events(mte, c);
				}
			}
			catch (ConnectException ce){
				try {
					ce.printStackTrace();
					//Removes the disconnected client from the server.
					int i = mySelf.getPeers().indexOf(disconnectedPeer);
					mySelf.removePeer(disconnectedPeer);
					for(RmiInterface c: mySelf.getPeers()){
						try {
							//Tells all clients to remove the disconnected client from their lists.
							c.removePeer(disconnectedPeer);
							c.removeUser(i);
							//Also continue sending events to those who did not get it.
							if(mySelf.getPeers().indexOf(c) > i-1 && lostEvent != null){
								events(lostEvent, c);
							}

						} catch (RemoteException e) {
							e.printStackTrace();
						}
					}
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
		}		
	}
	public void events(MyTextEvent mte, RmiInterface c) throws RemoteException{
		disconnectedPeer = c;
		lostEvent = mte;
		//Dont send or remove events if interrupted
		if(!interrupted){
			if (mte instanceof TextInsertEvent){
				//			System.out.println(mte.getSender());
				final TextInsertEvent tie = (TextInsertEvent)mte;
				if (c !=null){
					if(c.getName().equals(mySelf.getName())){ //Uses names, FIX!
						//Skip myself
					} else {
						c.sendEvent(tie);
					}
				}
			}
			else if (mte instanceof TextRemoveEvent){
				final TextRemoveEvent tre = (TextRemoveEvent)mte;
				if (c !=null){
					if(c.getName().equals(mySelf.getName())){ //Uses names, FIX!
						//Skip myself
					} else {
						c.removeEvent(tre);
					}
				}
			}
		}
	}
}
