import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class HistoryHandler {
	private ArrayList<MyTextEvent> myTextEventHistoryArrayList = new ArrayList<MyTextEvent>();
	private Map<String, Double> logicalClockCount = new HashMap<String, Double>();
	private String index = "hej";

	public void setIndex(String id){
		index = id;
		logicalClockCount.put(id, 0.0);
	}

	/**
	 * @param i 1(client) or 0(server)
	 * @return the logical clock timer count
	 */
	public double getLogicTime(String i){
		return logicalClockCount.get(i);
	}

	/**
	 * @return 1(client) or 0(server) 
	 */
	public String getIndex(){
		return index; 
	}

	/**
	 * Increases the logical clock counter by 1
	 */
	public synchronized void incrementTime(){

		logicalClockCount.put(index, getLogicTime(index)+1);
	}

	/**
	 * compares the 2 Clock counts and sets the max value to the local count
	 * @param paramArray the array to compare to
	 */
	public synchronized void updateLogicClock(Map<String, Double> param){
		for(String s : param.keySet()){
			logicalClockCount.put(s, Math.max(logicalClockCount.get(s), param.get(s)));
		}
	}

	/**
	 * get a partial arrayList based on upper and lower bounds
	 * @param s lower bound
	 * @param e upper bound
	 * @param index
	 * @return an array from s to e with myTextEvents
	 */
	public ArrayList<MyTextEvent> eventHistoryFromTo(double s, double e, String index){
		ArrayList<MyTextEvent> r = new ArrayList<MyTextEvent>();
		synchronized(myTextEventHistoryArrayList){
			for(MyTextEvent a : myTextEventHistoryArrayList){
				double time = a.getTime().get(index);
				if(time > s && time <= e){
					r.add(a);
				}
			}
			return r;
		}
	}

	/**
	 * Returns an arrayList holding all events
	 * @return  myTextEventHistoryArrayList
	 */
	public ArrayList<MyTextEvent> getEventHistory(){
		return myTextEventHistoryArrayList;
	}
	public void clearEventHistory(){
		myTextEventHistoryArrayList.clear();
	}
	public void clearClock(){
		logicalClockCount.clear();
	}

	public void addClientsToHistory(String id){
		logicalClockCount.put(id, 0.0);
	}

	/**
	 * @return current time
	 */
	public Map<String, Double> getTime(){
		Map<String, Double> temp = new HashMap<String,Double>(logicalClockCount);
		return temp;
	}
	/**
	 * adds MyTrxtEvents to the queue
	 * @param a MyTextEvents to add
	 */
	public void appendEvent(MyTextEvent a){
		synchronized(myTextEventHistoryArrayList){
			myTextEventHistoryArrayList.add(a);
		}
	}
}
