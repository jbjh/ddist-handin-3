import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class MyTextEvent implements Serializable{
	private Map<String, Double> time = new HashMap<String, Double>();
	private String sender;
	
	MyTextEvent(int offset) {
		this.offset = offset;
	}
	private int offset;
	int getOffset() { return offset; }
	
	public Map<String, Double> getTime(){
		return time;
	}
	/**
	 * 
	 * @param d Clock time to give the event
	 */
	public void setTime(Map<String, Double> d){
		time = d;
	}
	
	public String getSender(){
		return sender;
	}
	
	public void setOffset(int off){
		offset = off;
	}
	
	public void setSender(String s){
		sender = s;
	}
	public int getChange() {
		return 0;
	}
	
}