import java.rmi.RemoteException;

public class Pinger implements Runnable {

	private RmiInterface mySelf;
	private RmiInterface disconnectedPeer;
	public Pinger(RmiInterface mySelf){
		this.mySelf = mySelf;
	}
	public void run() {
		while(true){
			try {
				for(RmiInterface c : mySelf.getPeers()){
					disconnectedPeer = c;
					c.ping();
				}
				Thread.sleep(200);
			} catch (RemoteException e) {
				try {
					int i = mySelf.getPeers().indexOf(disconnectedPeer);
					mySelf.removePeer(disconnectedPeer);
					mySelf.removeUser(i);

				} catch (RemoteException e1) {
//					e1.printStackTrace();
				}
			} catch (InterruptedException e) {
//				e.printStackTrace();
			}
		}
	}
}
