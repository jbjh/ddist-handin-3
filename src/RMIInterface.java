import java.rmi.*;
import java.util.ArrayList;
 
public interface RmiInterface extends Remote{
	
	public String getIndex() throws RemoteException;
	public String getName() throws RemoteException;
	
	public void addUser(String name) throws RemoteException;
	public void removeUser(String name) throws RemoteException;
	public void removeUser(int i) throws RemoteException;
	public void updateUserList() throws RemoteException;
	
	public RmiInterface getPeer() throws RemoteException;
	public ArrayList<RmiInterface> getPeers() throws RemoteException;
	public void addPeer(RmiInterface c) throws RemoteException;
	public void removePeer(RmiInterface c) throws RemoteException;
		
	public String getHistory() throws RemoteException;
	
	//removes all references to previous connected users this is used to update users list when it disconnects
	public void removeAll() throws RemoteException;
	
	public int ping() throws RemoteException;
	
	public void sendEvent(TextInsertEvent tie) throws RemoteException;
	public void removeEvent(TextRemoveEvent tre) throws RemoteException;
	
}