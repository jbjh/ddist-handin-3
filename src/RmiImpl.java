import java.rmi.*;
import java.rmi.server.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;

import javax.swing.DefaultListModel;
import javax.swing.JTextArea;

public class RmiImpl extends UnicastRemoteObject implements RmiInterface  {

	public String name;
	public JTextArea area;
	public DefaultListModel<String> users;
	public RmiInterface client=null;
	private HistoryHandler myHistoryHandler;
	private String ownIndex;
	public ArrayList<RmiInterface> peers = new ArrayList<RmiInterface>();
	/**
	 * Constructor - Instantiates the RMI interface implementation, to handle different rmi objects.
	 * @param n
	 * @param area
	 * @param users
	 * @param myHistoryHandler
	 * @throws RemoteException
	 */
	public RmiImpl(String n, JTextArea area, DefaultListModel<String> users, HistoryHandler myHistoryHandler)  throws RemoteException { 
		this.name=n;   
		this.area = area;
		this.users = users;
		this.myHistoryHandler = myHistoryHandler;
	}
	/**
	 * Returns the index saved in the history handler.
	 */
	public String getIndex(){
		return myHistoryHandler.getIndex();
	}
	/**
	 * Returns the name of the peer
	 */
	public String getName() throws RemoteException {
		return this.name;
	}
	/**
	 * Adds a user to the user list on the right hand side.
	 * @param name
	 */
	public void addUser(String name) throws RemoteException{
		users.addElement(name);
	}
	/**
	 * Remove a user to the user list on the right hand side.
	 * @param name
	 */
	public void removeUser(String name) throws RemoteException{
		users.removeElement(name);
	}
	/**
	 * Remove a user to the user list on the right hand side.
	 * @param i, an array index
	 */
	public void removeUser(int i) throws RemoteException{
		users.remove(i);
	}
	/**
	 * Updates the userlist after inserting to the model
	 */
	public void updateUserList() throws RemoteException{
		for(RmiInterface c : peers){
			if(users.contains(c.getName())){
				//Skip if already in list
			} else {
				users.addElement(c.getName());
				myHistoryHandler.addClientsToHistory(c.getIndex());
			}
		}
	}
	/**
	 * Returns this peer object.
	 */
	public RmiInterface getPeer() throws RemoteException{
		return client;
	}
	/**
	 * Returns an array with all connected peers.
	 */
	public ArrayList<RmiInterface> getPeers() throws RemoteException {
		return peers;
	}
	/**
	 * Adds a peer to the list of connected peers.
	 */
	public void addPeer(RmiInterface c)  throws RemoteException{
		peers.add(c);
	}
	/**
	 * Removes a peer from the list of connected peers.
	 */
	public void removePeer(RmiInterface c) throws RemoteException{
		peers.remove(c);
	}
	/**
	 * Returns everything in the text field so far.
	 */
	public String getHistory(){
		return area.getText();
	}
	/**
	 * Clears everything, used for disconnecting.
	 */
	public void removeAll(){
		peers.clear();
		users.clear();
	}

	private ArrayList<MyTextEvent> sortEventHistory(Map<String, Double> time, String senderIndex){
		ownIndex = myHistoryHandler.getIndex();
		ArrayList<MyTextEvent> historyInterval = myHistoryHandler.eventHistoryFromTo(time.get(ownIndex), myHistoryHandler.getLogicTime(ownIndex), ownIndex);
		Collections.sort(historyInterval, new Comparator<MyTextEvent>() {
			public int compare(MyTextEvent o1, MyTextEvent o2) {
				return o1.getTime().get(ownIndex).compareTo(o2.getTime().get(ownIndex));
			}
		});
		return historyInterval;
	}
	/**
	 * Dummy method only for the exception to check the connectivity.
	 */
	public int ping() throws RemoteException {
		return 0;
	}
	/**
	 * Sending/inserting events method
	 */
	public void sendEvent(TextInsertEvent tie) throws RemoteException{
		synchronized(area.getDocument()){
		boolean skip = false; 
		final String fromIndex = tie.getSender();
		final Map<String, Double> time = tie.getTime();
		int offseter = tie.getOffset();
		ArrayList<MyTextEvent> myTextEventHistory = sortEventHistory(time, fromIndex);
		//Going through the eventqueue
		for(MyTextEvent my : myTextEventHistory){
//			System.out.println("historys offset " + my.getOffset());
			int listObjectOffset = my.getOffset();
			int listObjectChange = my.getChange();
			int myTextInsertEventOffset = tie.getOffset();
//			System.out.println(my.getTime().get(ownIndex));
			if(listObjectOffset == myTextInsertEventOffset && fromIndex.compareTo(ownIndex) < 0 && tie.getTime().get(fromIndex) == myHistoryHandler.getLogicTime(ownIndex) && tie.getTime().get(fromIndex) != 1){
				offseter = tie.getOffset()+1;
				tie.setOffset(offseter);
			}
			if(listObjectOffset <= myTextInsertEventOffset && (listObjectOffset != myTextInsertEventOffset || fromIndex.compareTo(ownIndex) < 0)){
				if(my instanceof TextRemoveEvent && listObjectOffset + ((TextRemoveEvent) my).getLength() > myTextInsertEventOffset){
					skip = true;
					((TextRemoveEvent) my).setLength(((TextRemoveEvent) my).getLength() + tie.getChange());
				}else{
//					System.out.println(listObjectChange);
					tie.setOffset(myTextInsertEventOffset + listObjectChange);}
			}else{my.setOffset(listObjectOffset + tie.getChange());}
		}
		

		//for writing received input without retransmitting it
		    myHistoryHandler.updateLogicClock(time);
			((TextAreaDocument) area.getDocument()).flipFilter();
//			System.out.println("TEXT: " + tie.getText()+" OFFSET: "+ offseter );
			tie.setOffset(offseter);
			area.insert(tie.getText(), offseter);
			int pos = area.getCaret().getDot();
			if(tie.getOffset() == pos &&  fromIndex.compareTo(ownIndex) < 0){
				area.getCaret().setDot(pos);
			}
			if(tie.getOffset() < pos){
				area.getCaret().setDot(pos+1);
			}
			((TextAreaDocument) area.getDocument()).flipFilter();
		}
	}
	/**
	 * 	Remove events method
	 */
	public void removeEvent(TextRemoveEvent tre) throws RemoteException{
		synchronized(area){
		boolean skip = false;
		final String fromIndex = tre.getSender();
		final Map<String, Double> time = tre.getTime();
		ArrayList<MyTextEvent> myTextEventHistory = sortEventHistory(time, fromIndex);
		//Going through the eventqueue
		for (MyTextEvent historyItem : myTextEventHistory) {
			int removeEventOffset = tre.getOffset();
			int removeEventTextLengthChange = tre.getChange();
			int removeEventLength = tre.getLength();
			int historyItemOffset = historyItem.getOffset();
			int historyItemTextLengthChange = historyItem.getChange();
			if(historyItemOffset == removeEventOffset && tre.getTime().get(fromIndex).equals(historyItem.getTime().get(fromIndex))){
				skip = true;
			}
			if(removeEventOffset >= area.getDocument().getLength()){
				skip = true;
			}
			if (historyItemOffset <= removeEventOffset && (historyItemOffset != removeEventOffset || fromIndex.compareTo(ownIndex) < 0)) {
				if (historyItem instanceof TextRemoveEvent && removeEventOffset <= historyItemOffset + ((TextRemoveEvent) historyItem).getLength() && (historyItemOffset + ((TextRemoveEvent) historyItem).getLength() != removeEventOffset || ownIndex.compareTo(fromIndex) < 0)) {
					tre.setLength(removeEventLength - (historyItemOffset + ((TextRemoveEvent) historyItem).getLength() - removeEventOffset));
					tre.setOffset(historyItem.getOffset());
				}
				else if (historyItem instanceof TextRemoveEvent && historyItemOffset + ((TextRemoveEvent) historyItem).getLength() >= removeEventOffset + removeEventLength && (historyItemOffset + ((TextRemoveEvent) historyItem).getLength() != removeEventOffset + removeEventLength || ownIndex.compareTo(fromIndex) < 0)){
					skip = true;
					((TextRemoveEvent)historyItem).setLength(((TextRemoveEvent) historyItem).getLength() + tre.getLength());
				}
				else {
					tre.setOffset(removeEventOffset + historyItemTextLengthChange);
				}
			} else if (historyItemOffset <= removeEventOffset && (historyItemOffset != removeEventOffset || fromIndex.compareTo(ownIndex) < 0)) {
				tre.setLength(removeEventLength + Math.max(historyItemTextLengthChange, -(removeEventOffset + removeEventLength - historyItemOffset)));

			} else {
				historyItem.setOffset(historyItemOffset + removeEventTextLengthChange);
			}
		}
		myHistoryHandler.updateLogicClock(time);
		int removeEventOffset = tre.getOffset();
		int removeEventLength = tre.getLength();

		//For handling received deletion inputs and not retransmitting them
		if (!skip){
			try{
//				System.out.println("offset position " + tre.getOffset() + " documentlength " + area.getDocument().getLength());
			((TextAreaDocument) area.getDocument()).flipFilter();
			area.replaceRange(null, removeEventOffset, removeEventOffset + removeEventLength);
			((TextAreaDocument) area.getDocument()).flipFilter();
			}catch(IllegalArgumentException iae){
//				System.out.println("System did something that it shouldnt");
			}
		}
		}
	}
}