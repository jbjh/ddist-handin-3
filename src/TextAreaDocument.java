import javax.swing.text.DocumentFilter;
import javax.swing.text.PlainDocument;

public class TextAreaDocument extends PlainDocument {
	private boolean filter = true;
	public void flipFilter(){
		filter ^= true;
	}
	public DocumentFilter getDocumentFilter(){
		return (filter) ? super.getDocumentFilter() : null;
	}
};